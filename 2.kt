import java.util.HashMap

fun <F, S> buildMutableMap(build: HashMap<F, S>.() -> Unit): Map<F, S> 
{
    var mp = HashMap<F, S>()
    mp.build()
    return mp
}
     

fun usage(): Map<Int, String> {
    return buildMutableMap {
        put(0, "0")
        for (i in 1..10) {
            put(i, "$i")
        }
    }
}
